import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BMIScaleComponent } from './scales/bmi-scale/bmi-scale.component';
import { GlasgowComaScaleComponent } from './scales/glasgow-coma-scale/glasgow-coma-scale.component';
import { ScalesComponent } from './scales/scales.component';
import { SearchComponent } from './search/search.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    {path: 'Home', component: SearchComponent},
    {path: '', redirectTo: 'Home', pathMatch:'full'},
    {path: 'All', component: ScalesComponent},
    {path: 'Glasgow', component: GlasgowComaScaleComponent},
    {path: 'BMI', component: BMIScaleComponent}
  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
