import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';

@Component({
  selector: 'app-scales',
  templateUrl: './scales.component.html',
  styleUrls: ['./scales.component.css']
})
export class ScalesComponent implements OnInit {
  input: string = '';
  list: any = [];
  i: number = 0;
  text: string = '';
  @ViewChildren("child") aView!: QueryList<ElementRef>;

  constructor() { }

  ngOnInit(): void {
  }

  filter(value:string) {
    this.input = value.toLowerCase();
    this.list = this.aView.toArray();

    if (this.input === '') {
      for (let i = 0; i < this.list.length; i++ ) {
        this.text = this.list[i].nativeElement.innerText;
        if (this.text.toLowerCase().indexOf(this.input) !== -1) {
          this.list[i].nativeElement.hidden = false;
        }
      }
    } else {
      for (let i = 0; i < this.list.length; i++ ) {
        this.text = this.list[i].nativeElement.innerText;
        if (this.text.toLowerCase().indexOf(this.input) === -1) {
          this.list[i].nativeElement.hidden = true;
        }
      }
    }
    
  }
}
