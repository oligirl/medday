import { ReturnStatement } from '@angular/compiler';
import { NgModuleCompileResult } from '@angular/compiler/src/ng_module_compiler';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-glasgow-coma-scale',
  templateUrl: './glasgow-coma-scale.component.html',
  styleUrls: ['./glasgow-coma-scale.component.css']
})
export class GlasgowComaScaleComponent implements OnInit {
  eyeValue: number = 0;
  verbalValue: number = 0;
  motorValue: number = 0;
  overAllScore: number = 0;
  value: string = "noResult";
 

  constructor() { 
  }

  ngOnInit(): void {
  }

  getEyeValue(value1: number){
    this.eyeValue = value1;
    return this.eyeValue;
  }

  getVerbalValue(value: number) {
    this.verbalValue = value;
    return this.verbalValue;
  }

  getMotorValue(value: number) {
    this.motorValue = value;
    return this.motorValue;
  }
  
  getScore(){
    if ((this.eyeValue > 0) && (this.verbalValue > 0) && (this.motorValue > 0)) {
      
      this.overAllScore = this.eyeValue + this.verbalValue + this.motorValue;

      if (this.overAllScore > 12) {
        this.value = "mild";
      } else if (this.overAllScore < 9 && this.overAllScore >2) {
        this.value = "severe";
      } else if (this.overAllScore < 13 && this.overAllScore > 8) {
        this.value = "moderate";
      }
    } else this.value = "noResult";

    return this.overAllScore;
  };
}
