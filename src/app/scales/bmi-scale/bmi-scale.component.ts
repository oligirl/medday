import { assertPlatform, Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-bmi-scale',
  templateUrl: './bmi-scale.component.html',
  styleUrls: ['./bmi-scale.component.css']
})
export class BMIScaleComponent implements OnInit {
  bmi: number = 0;
  weight: number = 0;
  height: number = 0;
  value: string = "noResult";

  constructor() { }

  ngOnInit(): void {
  }


  getBMI(){
    if ((this.weight > 0) && (this.height > 0) ) {

      this.bmi = (this.weight/((this.height/100)*(this.height/100)))*100;
      this.bmi = Math.round(this.bmi)/100;
  
      if (this.bmi < 18.5) {
        this.value = "underweight";
      } else if (this.bmi > 18.4 && this.bmi < 25) {
        this.value = "normal";
      } else if (this.bmi > 24.9 && this.bmi < 30) {
        this.value = "overweight";
      } else if (this.bmi > 29.9) {
        this.value = "obese";
      } 
    } else this.value = "noResult";
    return this.bmi;
  }
}
