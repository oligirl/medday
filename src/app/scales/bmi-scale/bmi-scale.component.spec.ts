import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BMIScaleComponent } from './bmi-scale.component';

describe('BMIScaleComponent', () => {
  let component: BMIScaleComponent;
  let fixture: ComponentFixture<BMIScaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BMIScaleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BMIScaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
